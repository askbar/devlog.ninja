$(function() {
    var ua = window.navigator.userAgent
    var msie = ua.indexOf ( "MSIE " )
    if ( msie > 0 )
    	$('#ie-not-supported').modal('show');
    else
    	$('#welcome').modal('show');

	$('.files').click(function(evt) {
		this.value = null;
	});
	$('.files').change(function(evt) {
		loopFiles();
		for (var i = 0; i < $('.files').length; i++) {
			listFiles($('.files')[i].files);
		}
		refreshFilebutton(this);
	});
	$('#menu').click(function() {
		$('#menu-content').fadeToggle('fast');
	});
});

function refreshFilebutton(elementFileButton) {
	$(elementFileButton).clone(true).insertBefore(elementFileButton);
	$(elementFileButton).hide();
}

/**
 * 
 */
function listFiles(files) {
	if (!files.length && !$(".files-list li").length) {
		$(".files-list").hide('fast');
		return;
	}
	$(".files-list").show('fast');
	$(files)
			.each(
					function(index, element) {
						var id = stripSpecialChars(element.name);
						if ($("#" + id).length < 1) {
							$(".files-list")
									.append(
											'<li id="'
													+ id
													+ '" class="list-group-item"><span class="badge">0</span><div>'
													+ element.name 
													+ getFileMenu(id)
													+ '</div></li>');
							updateFileMenuStatus(id, 'mute');
							updateFileMenuStatus(id, 'highlight');
						}
					});

	$('ul.files-list li, ul.files-list li div').hover(function(){
		$(this).find('div .fileMenu').fadeIn(100);
	}, function(){
		$(this).find('div .fileMenu').fadeOut(100);
	});
	$('.fileMenu a').click(function(e){
		e.preventDefault();
		var file = $(this).attr('data-file'), action = $(this).attr('data-action');
		setLSValue(file, action, !getLSValue(file, action));
		updateEntries($(this).attr('data-file'), $(this).attr('data-action'));
	})
}

function setLSValue(file, action, value) {
	localStorage[file + '.' + action] = JSON.stringify(value);
}

function getLSValue(file, action) {
	var val = localStorage[file + '.' + action];
	if (val === undefined)
		return false;
	else 
		return JSON.parse(val);
}

function updateEntries(file, action) {
	switch(action) {
		case 'mute':
			getLSValue(file, action) ? $('.entry[data-file=' + file + ']').hide() : $('.entry[data-file=' + file + ']').show();
			updateFileMenuStatus(file, action);
			break;
			
		case 'highlight':
			getLSValue(file, action) ? $('.entry[data-file=' + file + ']').addClass('highlight') : $('.entry[data-file=' + file + ']').removeClass('highlight');
			updateFileMenuStatus(file, action);
			break;
	}

}

function updateFileMenuStatus(file, action) {
	getLSValue(file, action) ? 
			$('a[data-file='+file+'][data-action='+action+']').removeClass("btn-default").addClass("btn-primary") :
			$('a[data-file='+file+'][data-action='+action+']').addClass("btn-default").removeClass("btn-primary");
}

function getFileMenu (id) {
	return '<div class="fileMenu">'
			+ '<a href="#" data-action="highlight" data-file="'+ id +'" title="highlight entries from this file" class="btn pull-right btn-default btn-xs" ><span class="glyphicon glyphicon-eye-open" /></a> '
			+ '<a href="#" data-action="mute" data-file="'+ id +'" title="mute entries from this file" class="btn pull-right btn-default btn-xs" ><span class="glyphicon glyphicon-remove" /></a>'
		+ '</div>';
}

/**
 * loop input tags and loop selected files within each
 */
function loopFiles() {

	var files = $('.files');
	for (var i = 0; i < files.length; i++) {
		$(files[i].files)
				.each(
						function(index, element) {

							var id = stripSpecialChars(element.name);
							var newFile, updateFile = false;
							var modified = parseInt(element.lastModified, 10);
							var modifiedDate = element.lastModifiedDate;
							var size = parseInt(element.size, 10);
							oldSize = 0;
							if (localStorage[id + '.lastModified'] == undefined) {
								newFile = true;
							} else if (parseInt(localStorage[id
									+ '.size'], 10) < size) {
								updateFile = true;
								oldSize = localStorage[id + '.size'];
							}

							// update file status
							localStorage[id + '.size'] = size;
							localStorage[id + '.lastModified'] = modified;
							localStorage[id + '.lastModifiedDate'] = modifiedDate;

							if (newFile) {
								readBlob(element, size - 512, size - 1);
							} else if (updateFile) {
								readBlob(element, oldSize, size - 1);
							}
						});
	}
	setTimeout(loopFiles, 300);
}

function readBlob(file, start, stop) {

	reader = new FileReader();

	// If we use onloadend, we need to check the readyState.
	reader.onloadend = function(evt) {
		if (evt.target.readyState == FileReader.DONE) { // DONE == 2
			// var lines =
			// evt.target.result.split(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g);
			var lines = evt.target.result.split(/\n/g);
			$(lines).each(
					function(indexLine) {
						if (lines[indexLine].length < 1) {
							return;
						}
						var id = 1;
						if (!isNaN($.cookie("entryId")))
							id = parseInt($.cookie("entryId"), 10) + 1;
						$('#entries').append(
								$('<div></div>').html('<img class="new-ball" src="/images/aqua_ball.png" />' + parseEntry(lines[indexLine])).attr(
										'data-file',
										stripSpecialChars(file.name)).attr(
										'class', 'entry').attr('data-time',
										file.lastModified).attr('id', id));
						$.cookie("entryId", id);
						
						newItemAdded(id);
					});
			entriesAdded();
		}
	};

	var blob = file.slice(start, stop + 1);
	reader.readAsBinaryString(blob);
}

function getFiles() {
	return $('.files')[0].files;
}

function stripSpecialChars(string) {
	return string.replace(/[^\w\s]/gi, '_');
}

function parseEntry(string) {
	return wrapIpAddress(string);
}

function wrapIpAddress(string) {
	return string;
	var ips = string.match(/(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/);
	if (ips.length) {
		string = string.replace(ips[0], '<span class="ip-number">' + ips[0] + '</span>');
	}
	return string;
}

function updateFilesEntriesCount() {
	var list = $('.files');
	for (var i = 0; i < list.length; i++) {
		$(list[i].files).each(
				function(index, element, list) {
					var count = $("[data-file="
							+ stripSpecialChars(element.name)
							+ "][data-age=new]").length;
					if (count > 0) {
						$('#' + stripSpecialChars(element.name) + ' .badge')
								.html(count).show('slow');
					} else {
						$('#' + stripSpecialChars(element.name) + ' .badge')
								.hide('slow');
					}
				});
	}
}

function entriesAdded() {
	updateFilesEntriesCount();
	$(".new-ball").addClass('fadeout');
	$("html, body").animate({ scrollTop: $(document).height() });
	cleanUpOldEntries();
}

function cleanUpOldEntries() {
	var max = 600;
	if ($('.entry').length > max) {	// we assume oldest is on top and remove those
		$('.entry:nth-child(n+0):nth-child(-n+' + ($('.entry').length - max) + ')').remove();
	}
}

function newItemAdded(id) {
	var entry = $('#' + id);
	!getLSValue(entry.attr('data-file'), 'highlight') ? false: entry.addClass("highlight");
	!getLSValue(entry.attr('data-file'), 'mute') ? false : entry.css('display', 'none');
	highlightErrors(entry);
	highlightWarnings(entry);
	entry.attr('data-age', 'new');
	setTimeout(function() {
		entry.attr('data-age', 'old');
		updateFilesEntriesCount();
	}, 30000);
	setTimeout(function() {
		entry.attr('data-age', 'older');
		updateFilesEntriesCount();
	}, 60000);
}

function highlightErrors(entry) {
	var ips = entry.text().match(/error|exception/i);
	if (ips==null)
		return;
	else if (ips.length) {
		string = entry.addClass('error');
	}
}
function highlightWarnings(entry) {
	var ips = entry.text().match(/warning/i);
	if (ips==null)
		return;
	else if (ips.length) {
		string = entry.addClass('warning');
	}
}

function getRandomColor() {
	var letters = '0123456789ABCDEF'.split('');
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}
